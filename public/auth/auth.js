// Signup
var _$ = document.querySelector.bind(document);

const signupForm = _$('#signup-form');
const signupFormWrapper = _$('#signup-wrapper');
const alertSignup = _$('#signup-wrapper .alert');
const loginWrapper = _$('#login-wrapper');
const logout = document.querySelector('#logout');
if (signupForm) {
    signupForm.addEventListener('submit', (e) => {
        e.preventDefault();
        // Get user Info
        const email = signupForm['signup-email'].value;
        const password = signupForm['signup-password'].value;
        // Signup the user
        auth.createUserWithEmailAndPassword(email, password)
            .then((res) => {
                signupForm.reset();
                alertSignup.classList.remove('d-none');
                alertSignup.innerHTML = "Registered successfully! You can login now";
                setTimeout(function () {
                    signupFormWrapper.classList.add('d-none');
                    loginWrapper.classList.remove('d-none');
                }, 2000);

                // document.querySelector('.error').innerHTML = '';
            }).catch(err => {
                alertSignup.classList.remove('d-none');
                alertSignup.classList.remove('alert-success');
                alertSignup.classList.add('alert-danger');
                alertSignup.innerHTML = err.message;
            });
    });
}



// Logout
if (logout) {
    logout.addEventListener('click', (e) => {
        e.preventDefault();
        //Logout the user
        auth.signOut().then(() => {
            console.log('User Signed Out');
        });
    });
}




// Login
const loginForm = document.querySelector('#login-form');
const loginFormAlert = document.querySelector('#login-wrapper .alert');
if (loginForm) {
    loginForm.addEventListener('submit', (e) => {
        e.preventDefault();
        const email = loginForm['login-email'].value;
        const password = loginForm['login-password'].value;
        auth.signInWithEmailAndPassword(email, password).then(cred => {
            loginForm.reset();
            console.log("Logged in");
            // loginForm.querySelector('.error').innerHTML = '';
        }).catch(err => {
            loginFormAlert.classList.remove('d-none');
            loginFormAlert.classList.remove('alert-success');
            loginFormAlert.classList.add('alert-danger');
            loginFormAlert.innerHTML = err.message;
        });
    });
}