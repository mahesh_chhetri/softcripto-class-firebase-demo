// create new guide
const categoryForm = document.querySelector('#category-form');
const categoryFormMessage = document.querySelector('#categoryform-wrapper .alert');
categoryForm.addEventListener('submit', (e) => {
    e.preventDefault();
    categoryFormMessage.classList.add('alert-success');
    categoryFormMessage.classList.add('d-none');
    categoryFormMessage.classList.remove('alert-danger');
    var categoryname = categoryForm['name'].value;
    // console.log("category",categoryname);
    db.collection('categories').add({
        name: categoryname
    }).then(() => {
        categoryFormMessage.classList.remove('d-none');
        categoryFormMessage.innerHTML = "Created as  <b>" + categoryname + "</b>";
        categoryForm.reset();
    }).catch(err => {
        categoryFormMessage.classList.remove('alert-success');
        categoryFormMessage.classList.add('alert-danger');
        categoryFormMessage.classList.remove('d-none');
        categoryFormMessage.innerHTML = err.message;
    });
});