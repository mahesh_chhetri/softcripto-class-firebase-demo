// create new guide
function getCategories() {
    db.collection('categories').get().then(snapshot => {
        var optionsElement = document.querySelector("#categories");
        snapshot.docs.forEach(function (category) {
            var option = document.createElement("option");
            option.setAttribute('value', category.id);
            option.innerHTML = category.data().name;
            optionsElement.appendChild(option);
        });
    });
}

getCategories();


const postForm = document.querySelector('#post-form');
const postFormMessage = document.querySelector('#postform-wrapper .alert');
postForm.addEventListener('submit', (e) => {
    e.preventDefault();
    postFormMessage.classList.add('alert-success');
    postFormMessage.classList.add('d-none');
    postFormMessage.classList.remove('alert-danger');
    var title = postForm['title'].value;
    var content = postForm['content'].value;
    var dropDown = postForm['categories']
    var categoryId = dropDown.value;
    var categoryName = dropDown.options[dropDown.selectedIndex].text;;
    db.collection('questions').add({
        title: title,
        hints: content,
        categoryId: "/categories/" + categoryId,
        categoryName: categoryName
    }).then(() => {
        postFormMessage.classList.remove('d-none');
        postFormMessage.innerHTML = "Create new post successfully!";
        postForm.reset();
    }).catch(err => {
        postFormMessage.classList.remove('alert-success');
        postFormMessage.classList.add('alert-danger');
        postFormMessage.classList.remove('d-none');
        postFormMessage.innerHTML = err.message;
    });
});