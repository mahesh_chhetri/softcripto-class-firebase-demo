var currentUrl = window.location.href;
var currentUser;
firebase.auth().onAuthStateChanged(function (user) {
    if (user) {
        currentUser = user;

        // console.log("User111", user);
        function isAdmin(userData) {
            return function (idTokenResult) {
                // console.log("hello23", user2);
                {
                    if (idTokenResult.claims.admin) {
                        userData['admin'] = idTokenResult.claims.admin;
                    }
                    // console.log("dddd", userData.admin);
                    var isAdmin = "";
                    if (userData.admin) {
                        isAdmin = "<b>(Admin)</b>";
                    }
                    if (!userData.admin) {
                        if (currentUrl.includes('admin-dashboard')) {
                            return window.location.href = "dashboard.html";
                        }
                    }

                    if (!currentUrl.includes('dashboard')) {
                        return window.location.href = "dashboard.html";
                    }

                    var user = document.querySelector('.username');
                    if (user) {
                        user.innerHTML = isAdmin + userData.email;
                    }
                }
            }
        }

        user.getIdTokenResult().then(isAdmin(user));






    } else {
        if (!currentUrl.includes('auth')) {
            window.location.href = "auth.html";
        }
    }
});