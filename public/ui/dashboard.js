// questions
function getPostLists() {
    db.collection('questions').get().then(function (snapshot) {
        var container = document.querySelector("#question-answer");
        var questionLength = snapshot.docs.length;
        snapshot.docs.forEach(function (question, index) {
            var setActiveFirst = index == 0 ? "show" : '';
            var questions = `<div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapse${question.id}"
                                        aria-expanded="true" aria-controls="collapseOne">
                                        Question.${index + 1}
                                    </button>
                                </h5>
                            </div>

                            <div id="collapse${question.id}" class="collapse ${setActiveFirst}" aria-labelledby="headingOne"
                                data-parent="#accordion">
                                <div class="card-body">
                                    <b>${question.data().title}</b>
                                    <form action="">
                                    <div class="alert alert-success d-none"></div>
                                        <input type="hidden" name="questionId" value="${question.id}">
                                        <textarea name="answer" class="form-control" id="" cols="30"
                                            rows="10"></textarea>
                                        <div class="form-group text-right">
                                            <input type="submit" class="btn btn-primary btn-block mt-2" value="Submit">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>`;

            // Apending
            // container.appendChild(question);
            container.innerHTML += questions;
            if (questionLength == index + 1) {
                answerFormEvents();
            }
            // console.log("posts", post.data());

        });
    });
}
getPostLists();




// Post answer
function answerFormEvents() {
    var _$$ = document.querySelectorAll.bind(document);
    const answerForms = _$$('#question-answer form');
    answerForms.forEach(function (formElement) {
        formElement.addEventListener('submit', function (e) {
            e.preventDefault();
            var data = {
                questionId: this['questionId'].value,
                answer: this['answer'].value,
                userId: currentUser.uid,
                isAnswered: true
            }
            var messageElement = this.querySelector(".alert");
            postAnswer(this, data, messageElement);
        });
    });

    function postAnswer(form, data, messageElement) {
        messageElement.classList.add('alert-success');
        messageElement.classList.add('d-none');
        messageElement.classList.remove('alert-danger');
        db.collection('answers').add(data).then(() => {
            messageElement.classList.remove('d-none');
            form.reset();
            messageElement.innerHTML = "Answer is submitted successfully";
        }).catch(err => {
            messageElement.classList.remove('alert-success');
            messageElement.classList.add('alert-danger');
            messageElement.classList.remove('d-none');
            messageElement.innerHTML = err.message;
        });
    }
}

