// create new guide
function getPostLists() {
    db.collection('questions').get().then(snapshot => {
        var container = document.querySelector("#postLists");
        snapshot.docs.forEach(function (post) {
            // li container
            var li = document.createElement("li");
            li.setAttribute("class", "list-group-item");
            // title
            var title = document.createElement("p");
            title.innerHTML = "<b>"+post.data().title+"</b>";
            // content
            var content = document.createElement("div");
            content.innerHTML = post.data().hints;
            // category tag
            var category = document.createElement("span");
            category.setAttribute("class", "tags");
            category.innerHTML = post.data().categoryName;
            // Apending
            li.appendChild(title);
            li.appendChild(content);
            li.appendChild(category);
            container.appendChild(li);
            console.log("posts", post.data());
        });
    });
}
getPostLists();