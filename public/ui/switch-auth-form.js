var _$ = document.querySelector.bind(document);
var goToLoginBtn = _$("#goToLogin");
var goToSignupBtn = _$("#goToSignup");
if (goToLoginBtn && goToSignupBtn) {
    goToLoginBtn.addEventListener("click", toggleForm);
    goToSignupBtn.addEventListener("click", toggleForm);
}

function toggleForm(e) {
    e.preventDefault();
    _$("#login-wrapper").classList.toggle("d-none");
    _$("#signup-wrapper").classList.toggle("d-none");
}